//
//  GoogleViewController.h
//  TabBar using objective-c
//
//  Created by OSX on 17/01/17.
//  Copyright © 2017 Ameba. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GoogleViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIWebView *googleWebView;

@end
