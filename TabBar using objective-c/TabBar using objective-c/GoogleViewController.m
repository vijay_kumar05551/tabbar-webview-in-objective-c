//
//  GoogleViewController.m
//  TabBar using objective-c
//
//  Created by OSX on 17/01/17.
//  Copyright © 2017 Ameba. All rights reserved.
//

#import "GoogleViewController.h"

@interface GoogleViewController ()

@end

@implementation GoogleViewController
@synthesize googleWebView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSURL *url = [NSURL URLWithString:@"https://www.google.com"];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    [googleWebView loadRequest:urlRequest];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
